#!/usr/bin/env bash

DIR=~/.system-backup
REPOS=/etc/yum.repos.d

cp -r "$REPOS" "$DIR"
sudo dnf history userinstalled | tail -n +2 >| "$DIR"/userinstalled.txt
