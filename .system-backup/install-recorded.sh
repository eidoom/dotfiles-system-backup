#!/usr/bin/env bash

DIR=~/.system-backup
REPOS=/etc/yum.repos.d

cp "$DIR"/"$(basename "$REPOS")"/* "$REPOS"/
sudo dnf install $(grep "^[^#]" "$DIR"/userinstalled.txt)
