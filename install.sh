#!/usr/bin/env bash

here=$(realpath "$0" | xargs dirname)
cd "$here" || exit

folder=.system-backup
mkdir -p ~/"$folder"
for file in "$folder"/*.sh; do
    ln -s "$here/$file" ~/"$file"
done
