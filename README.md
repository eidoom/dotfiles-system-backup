# [dotfiles-system-backup](https://gitlab.com/eidoom/dotfiles-system-backup)

## Description

Scripts to record repos and packages installed on my system, then reinstall them after OS reinstall or migrate setup to new machine.
Records kept in `~/.system-backup` and should be tracked by a private dotfiles git repo.
Supports Fedora 31.

## Example usage

Latest system packages list is recorded with `~/.system-backup/record-userinstalled.sh`.
This creates in `~/.system-backup` a directory `yum.repos.d` with installed `dnf` repositories and a file `userinstalled.txt` with installed packages.
These files are `git` pushed to the private dotfiles remote before OS reinstall, then the repo cloned to the new system.
This repo is also cloned and used to reinstall all packages with `~/.system-backup/install-recorded.sh`.

## Installation

### Manual install

* Install at `$HOME` with 
    * SSH clone (if you're me)
    ```shell
    git clone git@gitlab.com:eidoom/dotfiles-system-backup.git .dotfiles-system-backup
    ```
    * or HTTPS clone (if you're not me)
    ```shell
    git clone --depth 1 https://gitlab.com/eidoom/dotfiles-system-backup.git .dotfiles-system-backup
    ```
* then run the installation script
```shell
cd .dotfiles-system-backup
./install.sh
```

### Automated install

Alternatively, install as part of the [superproject](https://gitlab.com/eidoom/dotfiles-public).
